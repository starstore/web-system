function PriviToRole(privi) {
    switch(privi) {
        case 0:
        return '超级管理员';
        case 1:
        return '管理员';
        case 2:
        return '运营人员';
    }
}
function RoleToPrivi(role) {
    switch(role) {
        case '超级管理员':
        return 0;
        case '管理员':
        return 1;
        case '运营人员':
        return 2;
    }
}

function ActivesToString(actives) {
    switch(actives) {
        case 0:
        return '低';
        case 1:
        return '中';
        case 2:
        return '高';
    }
}
function ActivesToInt(actives) {
    if (actives == '低') {
        return 0;
    } else if (actives == '中') {
        return 1;
    } else if (actives == '高') {
        return 2;
    }
}

function FanunitToString(fanunit) {
    switch(fanunit) {
        case 0:
        return 'K';
        case 1:
        return 'W';
        case 2:
        return 'M';
    }
}
function FanunitToInt(fanunit) {
    switch(fanunit) {
        case 'K':
        return 0;
        case 'W':
        return 1;
        case 'M':
        return 2;
    }
}

function PriceTypeToString(price_type) {
    switch(price_type) {
        case 0:
        return 'CPC';
        case 1:
        return 'CPA';
        case 2:
        return 'PACKAGE';
    }
}
function PriceTypeToInt(price_type) {
    switch(price_type) {
        case 'CPC':
        return 0;
        case 'CPA':
        return 1;
        case 'PACKAGE':
        return 2;
    }
}


export default {
    RoleToPrivi,
    PriviToRole,
    ActivesToString,
    ActivesToInt,
    FanunitToString,
    FanunitToInt,
    PriceTypeToString,
    PriceTypeToInt
}