import Vue from "vue";
import VueRouter from "vue-router";
// import '../theme/index.css';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-default/index.css';
import './index.css';
import 'font-awesome/css/font-awesome.css';
import VueResource from 'vue-resource';
import locale from 'element-ui/lib/locale/lang/en'
// import echarts from 'echarts';
//pc
import PApp from './components/pc/App';
import PRegister from './components/pc/Register';
import PRegisterProduct from './components/pc/RegisterProduct';
import PLogin from './components/pc/Login';
import POnMsgList from './components/pc/OnMsgList';
import POffMsgList from './components/pc/OffMsgList';
import PNewUser from './components/pc/newUser';
import PChangePass from './components/pc/changePass';
import PProductList from './components/pc/productList';
import PInfluenserList from './components/pc/influenserList';
import PSourceData from './components/pc/SourceData';
import DashBord from './components/pc/dash/DashBord';
import CheckProduct from './components/pc/dash/CheckProduct';
import CheckCompany from './components/pc/dash/CheckCompany';
import CheckFinance from './components/pc/dash/CheckFinance';
import InviteList from './components/pc/influencer/InviteList';
import Upcoming from './components/pc/dash/Upcoming';
import ProductList from './components/pc/dash/ProductList';
import CompanyList from './components/pc/dash/CompanyList';
import TypeDetails from './components/pc/dash/TypeDetails';
import BillReport from './components/pc/dash/BillReport';
import Test from './components/pc/company/Test';
import Company from './components/pc/company/Company';
import CompanyDetails from './components/pc/company/CompanyDetails';
import AddNewCompany from './components/pc/company/AddNewCompany';
import ProductDetails from './components/pc/company/ProductDetails';
import AddProduct from './components/pc/company/AddProduct';
import NewInfluencer from './components/pc/influencer/Newinfluencer';
import NewInfluDetails from './components/pc/influencer/NewInfluDetails'
import InfluencerList from './components/pc/influencer/InfluenserList';
import NewInfluList from './components/pc/influencer/NewInfluList';
import IcampaignDetails from './components/pc/influencer/CampaignDetails';
import AddInfluencer from './components/pc/influencer/AddInfluencer';
import InfluencerDetails from './components/pc/influencer/InfluencerDetails';
import PostReview from './components/pc/influencer/PostReview';
import Gardenscapes from './components/pc/influencer/Gardenscapes';
import CreateCampaign from './components/pc/campaigns/CreateCampaign';
import Campaigns from './components/pc/campaigns/Campaigns';
import CampaignClose from './components/pc/campaigns/CampaignClose';
import UpdateCampaign from './components/pc/campaigns/UpdateCampiagn';
import Money from './components/pc/money/Money';
import SystemManage from './components/pc/system/SystemManage';
import {
    auth
} from './apiRequest.js';
import store from 'store2';

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(ElementUI,{locale});
// Vue.use(echarts);
let routes = [{
        path: '/app',
        name: 'app',
        component: PApp,
        children: [
            { path: 'onmsgList', component: POnMsgList },
            { path: 'offMsgList', component: POffMsgList },
            { path: 'sourceData', component: PSourceData },
            { path: 'register', component: PRegister },
            { path: 'registerProduct', component: PRegisterProduct },
            { path: 'productList', component: PProductList },
            { path: 'influenserList', component: PInfluenserList },
            { path: 'dashBord', component: DashBord },
            { path: 'checkProduct/:id', component: CheckProduct },
            { path: 'checkCompany/:id', component: CheckCompany },
            { path: 'checkFinance', component: CheckFinance },
            { path: 'upcoming', component: Upcoming },
            { path: 'dashProductList', component: ProductList },
            { path: 'dashCompanyList', component: CompanyList },
            { path: 'company', component: Company },
            { path: 'companyDetails/:id', component: CompanyDetails },
            { path: 'addNewCompany', component: AddNewCompany },
            { path: 'productDetails/:id', component: ProductDetails },
            { path: 'addProduct/:id', component: AddProduct },
            { path: 'influencerList', component: InfluencerList },
            { path: 'addInfluencer', component: AddInfluencer },
            { path: 'icampaignDetails/:id', component: IcampaignDetails },
            { path: 'influencerDetails/:id', component: InfluencerDetails },
            { path: 'money', component: Money },
            { path: 'systemManage', component: SystemManage },
            { path: 'newInfluList', component: NewInfluList },
            { path: 'postReview', component:PostReview},
            { path: 'campaigns', component:Campaigns},
            { path: 'campaignsClose', component:CampaignClose},
            { path: 'createCampaign', component:CreateCampaign},
            { path: 'updateCampaign/:id', component:UpdateCampaign},
            { path: 'gardenscapes/:id', component:Gardenscapes},
            { path: 'newinflu', component:NewInfluencer},
            { path: 'influDetail/:id', component:NewInfluDetails},
            { path: 'inviteList',component:InviteList},
            { path: 'typeDetails/:id/:date/:type/:name',component:TypeDetails},
            { path: 'billReport',component:BillReport},
          { path: 'test',component:Test},
        ]
    },
    { path: '/login', component: PLogin },
    { path: '/newUser', component: PNewUser },
    { path: '/changePass', component: PChangePass },
    { path: '/', redirect: '/login' }
];
const router = new VueRouter({
    // mode: 'history',
    hashbang: false, //将路径格式化为#!开头
    history: true, //启用HTML5 history模式
    abstract: false, //使用一个不依赖于浏览器的浏览历史虚拟管理后端
    transitionOnLoad: false, //初次加载是否启用场景切换
    saveScrollPosition: false, //在启用html5 history模式的时候生效，用于后退操作的时候记住之前的滚动条位置
    linkActiveClass: "v-link-active", //链接被点击时候需要添加到v-link元素上的class类,默认为active
    routes: routes
});

router.beforeEach((to, from, next) => {
  if (to.path == '/login') {
      next();
      return ;
  }
  let token = store.get('token');
  auth({token:token}).then((data)=>{
    if (data.data.code === 0) {
        next();
    } else {
        next('/login');
    }
  }).catch((e)=>{
    next('/login');
  });
});

new Vue({
    el: '#app',
    router
});
