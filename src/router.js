import Vue from "vue";
import VueRouter from "vue-router";
import App from './App';
import Bar from './components/Bar';
import Foo from './components/Foo';

Vue.use(VueRouter);

const routes = [{
        path: '/app',
        name: 'app',
        component: App,
        children: [
            { path: 'bar', component: Bar },
            { path: 'foo', component: Foo }
        ]
    },
    { path: '*', component: App }
]

const router = new VueRouter({
    mode: 'history',
    routes: routes
})


export default router
