import axios from "axios";
import store from 'store2';
import qs from 'qs';
import baseURL from './configUrl.js';

function baseRequest(method, path, params, data, type) {
    method = method.toUpperCase() || 'GET';
    let url = '';
    let paramsobj = { params: params };
    if (type === 'msg') {
        url = baseURL.onbaseURL;
    } else {
        url = baseURL.baseURL;
    }
    axios.defaults.baseURL = url;
    //axios.defaults.withCredentials = true;
    axios.defaults.headers['access-token'] = localStorage.getItem('token');
    if (method === 'POST') {
        axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        let o=axios.post(path, qs.stringify(data));
        o.then((data)=>{
          if(data.data.code==99){
            // 跳转到登录界面
            window.location.href='/#/login';
          }else{
          }
        })
        return o
    } else if (method === 'GET') {
        let o=axios.get(path, paramsobj);
        o.then((data)=>{
          if(data.data.code==99){
            // 跳转到登录界面
            window.location.href='/#/login';
          }else{
          }
        })
      return o
    } else {
        return axios.delete(path, qs.stringify(data));
    };
}

exports.baseURL = baseURL.baseURL;

exports.registerInfluencer = function registerInfluencer(data) {
    return baseRequest('POST', '/influencer', '', data);
};

exports.registerInfluencerLink = function registerInfluencerLink(data) {
    return baseRequest('POST', '/influencer_link_product', '', data);
};

exports.registerProduct = function registerProducts(data) {
    return baseRequest('POST', '/product', '', data);
};

exports.getProductList = function productList(params) {
    return baseRequest('GET', '/product', params, '');
};

exports.getMsgListRequest = function msgListRequest(params) {
    return baseRequest('GET', '/statistics', params, '', 'msg');
};

exports.getInfluencerPid = function influencerPid(pid) {
    return baseRequest('GET', '/influencer_product_assoc/' + pid, '', '', '');
};

exports.getInfluencer = function influencer() {
    return baseRequest('GET', '/influencer', '', '', '');
};
// updata influenser
exports.updateInfluencer = function updateInfluencer(data) {
    return baseRequest('POST', '/update_influencer', '', data);
};
exports.updateInfluencerLink = function updateInfluencerLink(data) {
    return baseRequest('POST', '/update_influencer_link_product', '', data);
};
// remove influenser
exports.removeInfluenser = function removeInfluenser(id) {
    return baseRequest('GET', '/del_influencer/' + id, '', '');
};
// exports.removeInfluenserLink = function removeInfluenserLink(id) {
//     return baseRequest('GET', '/del_influencer_link_product/'+id, '', '');
// };
// updata product
exports.updateProduct = function updateProduct(data) {
    return baseRequest('POST', '/update_product', '', data);
};
// remove product
exports.removeProduct = function removeProduct(id) {
    return baseRequest('GET', '/del_product/' + id, '', '');
};

// ck 去重
exports.getCkHourly = function ckHourly(params) {
    return baseRequest('GET', '/ck_hourly_1', params, '');
};
// pb 去重
exports.getPbHourly = function pbHourly(params) {
    return baseRequest('GET', '/pb_hourly_1', params, '');
};
// ck 去重
exports.getCkHourly1 = function ckHourly(params) {
    return baseRequest('GET', '/ck_hourly', params, '');
};
// pb 去重
exports.getPbHourly1 = function pbHourly(params) {
    return baseRequest('GET', '/pb_hourly', params, '');
};
// 来源
exports.getCkFromDaily = function ckFromDaily(params) {
    return baseRequest('GET', '/ck_from_daily', params, '');
};
// name 获取关联 id
exports.getContactId = function contactId(params) {
    return baseRequest('GET', '/get_influ_product_assoc', params, '');
};
exports.getCompaigns = function compaigns(params) {
    return baseRequest('GET', '/brand/campaignbystate/' + params, '', '');
};
exports.getCompaignDetails = function compaignDetails(params) {
    return baseRequest('GET', '/brand/campaignbyid/' + params, '', '');
};
exports.checkProduct = function checProduct(data) {
    return baseRequest('POST', '/brand/change-campaign-state', '', data);
};
exports.submitProduct = function subProduct(data) {
    return baseRequest('POST', '/opration/campaign', '', data);
};
exports.submitCountryProduct = function subcuntryProduct(data) {
    return baseRequest('POST', '/brand/mode_country', '', data);
};
exports.getbrandList = function brandList(params) {
    return baseRequest('GET', '/operation/company', params, '');
};
exports.getbrandDetails = function brandDetails(params) {
    return baseRequest('GET', '/operation/companydetail', params, '');
};
exports.getinfluenceList = function influenceList(params) {
    return baseRequest('GET', '/operation/influ', params, '');
};
exports.getinfluenceList2 = function influenceList2(params) {
    return baseRequest('GET', '/operation/influ2', params, '');
};
exports.influenceContactProduct = function influencecontact(params) {
    return baseRequest('GET', '/operation/distr_influ', params, '');
};
exports.addInfluencer = function addinflu(data) {
    return baseRequest('POST', '/operation/add_influ', '', data);
};
exports.getInflurncerDetail = function influencedetail(params) {
    return baseRequest('GET', '/operation/detail_influ', params, '');
};
exports.updateInfluencerDetail = function updateinflu(data) {
    return baseRequest('POST', '/operation/update_influ', '', data);
};

exports.getInflurncerDetailProduct = function influrncerDetailProduct(params) {
    return baseRequest('GET', '/operation/get_taked_campaign', params, '');
};

exports.createCompany = function createCompanys(data) {
    return baseRequest('POST', '/operation/add_company', '', data);
};

exports.updateCompany = function updataCompanys(data) {
    return baseRequest('POST', '/operation/update_company', '', data);
};

exports.getCompanyBaseMsg = function getCompanyBase(params) {
    return baseRequest('GET', '/operation/get_company_detail_all', params, '');
};
exports.takeinfluencerDetails = function influencerdetails(params) {
    return baseRequest('GET', '/influencer/influ_taked_campaings', params, '');
};
exports.checkCompany = function checkcompany(params) {
    return baseRequest('GET', '/operation/update_company_state', params, '');
};
//获取产品详情
export let getTakeCampaign = function(params) {
    return baseRequest('GET', '/influencer/influ_campaign_taked_detail', params, '');
};
export let getMyTakeCampaign = function(params) {
    return baseRequest('GET', '/influencer/influ_campaign_taked_detail2', params, '');
};

export let getInfluByCampaignId = function(params) {
    return baseRequest('GET', '/operation/get_influ_by_campaign', params, '');
};

export let getCampaignDetails = function(params) {
    return baseRequest('GET', '/operation/get_campaign_detail', params, '');
};
// 停止
export let stopCampiagn = function(data) {
    return baseRequest('POST', '/operation/stop_campaign', '', data);
};

export let stopCampiagnByInflu = function(data) {
    return baseRequest('POST', '/operation/stop_campaign_byinflu', '', data);
};

// 创建产品
export let createCompanyProducts = function(data) {
    return baseRequest('POST', '/operation/add_campaign', '', data);
};
export let createCampaigns = function(data) {
  return baseRequest('POST', '/api/1.0/operation/campaign/add', '', data);
};

export let updateCampaigns = function(data) {
    return baseRequest('POST', '/api/1.0/operation/campaign/update', '', data);
};

export let getBrandUserMsg = function(params) {
    return baseRequest('GET', '/brand/get_brand_user', params, '');
};

export let getCompaignsById = function(params) {
    return baseRequest('GET', '/brand/campaignbyid/' + params, '', '');
};

export let setInfluencerSocial = function(data) {
    return baseRequest('POST', '/influencer/set_influ_user_socialplatform', '', data);
};

export let getCompaignsState = function(params) {
    return baseRequest('GET', '/operation/get_campaign_state', params, '');
};

export let setCompaignsState = function(params) {
    return baseRequest('GET', '/operation/set_campaign_state', params, '');
};

export let getacssIdByInfluId = function(params) {
    return baseRequest('GET', '/influencer/get_influ_campaign_id', params, '');
};

export let getPhoneUnic = function(params) {
    return baseRequest('GET', '/brand/phone/' + params, '', '');
};

export let getMyInfluencerList = function(params) {
    return baseRequest('GET', '/influencer/influ_taked_campaings', params, '');
};

export let getInfluencerWaleterList = function(params) {
    return baseRequest('GET', '/influencer/get_influ_wallet_by_state', params, '');
};

export let updataWaleter = function(params) {
    return baseRequest('GET', '/influencer/set_influ_wallet_state', params, '');
};

export let setInfluencerPlatform = function(data) {
    return baseRequest('POST', '/operation/set_influ_platform', '', data);
};

export let updateInfluencerPlatform = function(data) {
    return baseRequest('POST', '/operation/update_influ_platform', '', data);
};

export let getInfluencerSocial = function(params) {
    return baseRequest('GET', '/influencer/get_influ_user_socialplatform', params, '');
};

export let getCompaignsList = function(params) {
    return baseRequest('GET', '/operation/get_campaigns', params, '');
};

export let getCompaignDetailsById = function(params) {
    return baseRequest('GET', '/operation/get_campaign_by_id', params, '');
};

export let getCompaignDetailsByIdData = function(params) {
    return baseRequest('GET', '/operation/get_campaign_daily', params, '');
};

export let setCountryPriceList = function(data) {
    return baseRequest('POST', '/operation/set_operation_campaign_country_price', '', data);
};

export let getAbnameExist = function(params) {
    return baseRequest('GET', '/operation/check_campaign_abname', params, '');
};

export let login = function(params) {
    return baseRequest('POST', '/operation/login', '', params);
};
export let auth = function(params) {
    return baseRequest('GET', '/operation/auth', params, '');
};
export let getNewInfluNum = function(params) {
    return baseRequest('GET', '/operation/new_influ_num', params, '');
};
export let getNewInfluList = function(params) {
    return baseRequest('GET', '/operation/new_influ_list', params, '');
};
export let getOperationUserList = function(params) {
    return baseRequest('GET', '/operation/operation_user_list', params, '');
};
export let reviewInflu = function(params) {
    return baseRequest('POST', '/operation/review_influ', '', params);
};
export let addOperationUser = function(params) {
    return baseRequest('POST', '/operation/add_operation_user', '', params);
};
export let updateCampaign = function(params) {
    return baseRequest('POST', '/operation/update_operation_campaign', '', params);
};
export let update_campaign_country = function(params) {
    return baseRequest('POST', '/operation/update_campaign_country', '', params);
};
export let update_campaign_perf = function(params) {
    return baseRequest('POST', '/operation/update_campaign_perf', '', params);
};
export let update_campaign_materail = function(params) {
    return baseRequest('POST', '/operation/update_campaign_material', '', params);
};
export let add_campaign_country = function(params) {
    return baseRequest('POST', '/operation/add_campaign_country', '', params);
};
export let del_campaign_country = function(params) {
    return baseRequest('GET', '/operation/del_campaign_country', params, '');
};
export let get_campaign_country_price = function(params) {
    return baseRequest('GET', '/operation/get_operation_campaign_country_price', params, '');
};
export let update_campaign_country_price = function(params) {
    return baseRequest('POST', '/operation/update_campaign_country_price', '', params);
};
export let del_campaign_country_price = function(params) {
    return baseRequest('GET', '/operation/del_campaign_country_price', params, '');
};
export let add_campaign_country_price = function(params) {
    return baseRequest('POST', '/operation/add_campaign_country_price', '', params);
};
//提交POST LINK
export let put_influencer_postlink = function(params) {
  return baseRequest('POST', '/operation/put_influencer_postlink', '', params);
};
//获取旧密码
export let get_user_pass = function(params) {
  return baseRequest('POST', '/operation/setpwd', '', params);
};
//campaigns列表
export let get_campaigns = function(params) {
  return baseRequest('GET', '/api/1.0/operation/campaign/get',  params ,'');
};
export let get_close_campaigns = function(params) {
    return baseRequest('GET', '/api/1.0/operation/campaign/getendcp', params ,'');
  };
export let getCampaignDetail = function(params) {
    return baseRequest('GET', '/api/1.0/operation/campaign/detail',  params,'');
};
// 查询campaign历史列表
export let getCampaignHistory = function(params) {
  return baseRequest('GET', '/api/1.0/operation/campaign/campaignhistorylist',  params,'');
};
//历史详情
export let getCamhisinfo = function(params) {
  return baseRequest('GET', '/api/1.0/operation/campaign/camhisinfo',  params,'');
};
// 邀请网红
export let inviteinflu = function(params) {
  return baseRequest('POST', '/api/1.0/operation/campaign/inviteinflu', '', params);
};
//查询网红
export let searchInflu = function(params) {
  return baseRequest('GET', '/operation/influ',  params,'');
};
//运营端更改campaign状态
export let changeStatus = function(params) {
  return baseRequest('POST', '/api/1.0/operation/campaign/upstatus', '', params);
};

export let campaignInflu = function(params) {
  return baseRequest('GET', '/api/1.0/operation/campaign/campaigninflu',  params,'');
};

export let campaignReport = function(params) {
  return baseRequest('GET', '/api/1.0/operation/campaign/campaignreport',  params,'');
};
// 查询网红列表
export let influList = function(params) {
  return baseRequest('GET', '/api/1.0/operation/influencer/influ',  params,'');
};

export let cpinvite = function(params) {
  return baseRequest('GET', '/api/1.0/operation/campaign/getcpinvitelist',  params,'');
};

export let priveCp = function(params) {
  return baseRequest('GET', '/api/1.0/operation/campaign/privecpinflulist',  params,'');
};
// 回填posturl
export let addSocial = function(params) {
  return baseRequest('POST', '/api/1.0/operation/campassoc/addsocialmd', '', params);
};
//删除posturl
export let delSocial = function(params) {
  return baseRequest('POST', '/api/1.0/operation/campassoc/delcpassocsocmd', '', params);
};
// 邀请网红注册
export let inviteInflu = function(params) {
  return baseRequest('POST', '/api/1.0/operation/influencer/inviteinflureg', '', params);
};
//网红详情
export let influInfo = function(params) {
  return baseRequest('GET', '/api/1.0/operation/influencer/infuinfo',  params,'');
};

export let inviteList = function(params) {
  return baseRequest('GET', '/api/1.0/operation/influencer/invitelist',  params,'');
};
export let resendInvite = function(params) {
  return baseRequest('GET', '/api/1.0/operation/influencer/resendinvite',  params,'');
};
export let gettongji = function(params) {
  return baseRequest('GET', '/api/1.0/operation/influencer/gettongji',  params,'');
};
// BillReport cpa
export let captotal = function(params) {
  return baseRequest('GET', '/api/1.0/operation/billreport/cpatotal',  params,'');
};
export let getcpadetail = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/getcpadetail', '', params);
};
export let editbilldetail = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/editbilldetail', '', params);
};
// total查询
export let gettotaldeduction = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/gettotaldeduction', '', params);
};
// total修改
export let edittotaldeduct = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/edittotaldeduct', '', params);
};

//获取campaign列表
export let getinflucp = function(params) {
  return baseRequest('GET', '/api/1.0/operation/influcampaign/getinflucp',  params,'');
};
//BillReport cps
export let cpstotal = function(params) {
  return baseRequest('GET', '/api/1.0/operation/billreport/cpstotal',  params,'');
};
export let getcpsdetail = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/getcpsdetail', '', params);
};
export let getcpstotaldeduction = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/getcpstotaldeduction', '', params);
};
export let editcpsbilldetail = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/editcpsbilldetail', '', params);
};
export let editcpstotaldeduct = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/editcpstotaldeduct', '', params);
};
// BillReport package
export let getlmpacendcp = function(params) {
  return baseRequest('GET', '/api/1.0/operation/influcampaign/getlmpacendcp',  params,'');
};
export let packagetotal = function(params) {
  return baseRequest('GET', '/api/1.0/operation/billreport/packagetotal',  params,'');
};
export let getpackagedetail = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/getpackagedetail', '', params);
};
export let getpackagetdeduc = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/getpackagetdeduc', '', params);
};
export let editpackagededuc = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/editpackagededuc', '', params);
};
// type detail更改审核状态
export let flagbillreport = function(params) {
  return baseRequest('POST', '/api/1.0/operation/billreport/flagbillreport', '', params);
};

