'use strict';
const url = require("url");
module.exports = {
    shouldInterceptHttpsReq :function(req){
        return true;
    },
    replaceResponseStatusCode: function(req,res,statusCode){
        // var uri = url.parse(req.url);
        //  console.log(url.parse(req.url));
        //  if(/\/api*/.test(uri.pathname)){
        //     statusCode = 200;
        //  //     return callback('{}');
        //  }
         // statusCode = 200;
         return statusCode;
     },
     replaceServerResDataAsync: function(req,res,serverResData,callback){
         // var uri = url.parse(req.url);
         // if(/\/api*/.test(uri.pathname)){
         // return callback(`{
         //        "data":{
         //             "token":"klfajlhkjjljdkaflj",
         //             "timeOut":17889987655
         //         }
         //
         //    }`);
         // }
         callback(serverResData);
     },
    shouldUseLocalResponse : function(req,reqBody){
        if(req.method == "OPTIONS"){
            return true;
        }else{
            return false;
        }
    },
    dealLocalResponse : function(req,reqBody,callback){
        if(req.method == "OPTIONS"){
            callback(200,mergeCORSHeader(req.headers),"");
        }
    },
    replaceResponseHeader: function(req,res,header){
        return mergeCORSHeader(req.headers, header);
    }
};
function mergeCORSHeader(reqHeader,originHeader){
    var targetObj = originHeader || {};

    delete targetObj["Access-Control-Allow-Credentials"];
    delete targetObj["Access-Control-Allow-Origin"];
    delete targetObj["Access-Control-Allow-Methods"];
    delete targetObj["Access-Control-Allow-Headers"];

    targetObj["access-control-allow-credentials"] = "true";
    targetObj["access-control-allow-origin"]      = reqHeader['origin'] || "-___-||";
    targetObj["access-control-allow-methods"]     = "GET, POST, PUT";
    targetObj["access-control-allow-headers"]     = reqHeader['access-control-request-headers'] || "-___-||";

    return targetObj;
}